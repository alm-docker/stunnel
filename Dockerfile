FROM multiarch/alpine:_DISTCROSS-edge
MAINTAINER Adrien le Maire <adrien@alemaire.be>
COPY docker-entrypoint.sh /usr/local/bin/
ARG VERSION
COPY docker-entrypoint.sh /entrypoint.sh
RUN apk add --no-cache stunnel=$VERSION libressl && chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
